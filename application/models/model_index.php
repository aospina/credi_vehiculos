<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class model_index extends CI_Model {

	public function ciudades(){
		$this->db->order_by('ciudad','asc');
      	$query=$this->db->get('ciudades');
	     if ($query->num_rows>0){
	      return $query->result_array();
	     } else {
	      return false;
	     }
  	}

  	public function ocupaciones(){
		$this->db->order_by('ocupacion','asc');
      	$query=$this->db->get('ocupaciones');
	     if ($query->num_rows>0){
	      return $query->result_array();
	     } else {
	      return false;
	     }
  	}

  	public function Insert_registro($data){
		 return $this->db->insert('registro',$data);
  	}

  	public function user($data){
      $query=$this->db->query("SELECT * FROM user where user='".$data['user']."'
       and password='".$data['password']."' ");
        if ($query->num_rows>0) {
          return $query->result();
        }else{
          return false;
        }
     }


      public function user_total(){
      $query=$this->db->query("SELECT * FROM user WHERE user.tipo <> 1");
        if ($query->num_rows>0) {
          return $query->result_array();
        }else{
          return false;
        }
     }


  public function insert_user($datos){
       return $this->db->insert('user',$datos);
  }

  public function eliminar_user($id_user){
    $this->db->where('id',$id_user);
       return $this->db->delete('user');
  }

  public function update_user($datos){
      $this->db->where('id',$datos['id']);
       return $this->db->update('user',$datos);
  }

    

    public function verificacion_user($user){
      $query=$this->db->query("SELECT * FROM user where user='".$user."'");
        if ($query->num_rows>0) {
          return false;
        }else{
          return true;
        }
  }

  public function verificacion_userEditar($user,$id){
      $query=$this->db->query("SELECT * FROM user where user='".$user."' and id!=".$id);
        if ($query->num_rows>0) {
          return false;
        }else{
          return true;
        }
  }

  
   public function user_id($id){
      $this->db->where('id',$id);
      $query=$this->db->get('user');
     if ($query->num_rows>0){
      return $query->result();
     } else {
      return false;
     }
  }

  public function registros(){
      $this->db->select('*');
      $this->db->join('ciudades', 'ciudades.id = registro.id_ciudad', 'inner');
      $this->db->join('ocupaciones', 'ocupaciones.id = registro.id_ocupacion', 'inner');
      $query=$this->db->get('registro');
     if ($query->num_rows>0){
      return $query->result_array();
     } else {
      return false;
     }
  }

      

   public function registro_like($palabra){
      $query=$this->db->query("SELECT * FROM registro
          INNER JOIN ciudades ON ciudades.id = registro.id_ciudad
          INNER JOIN ocupaciones ON ocupaciones.id = registro.id_ocupacion
          where registro.nombre like '%".$palabra."%' OR registro.apellido like '%".$palabra."%'
          OR registro.correo like '%".$palabra."%' or registro.telefono like '%".$palabra."%'
          or registro.celular like '%".$palabra."%' or registro.marca_carro like '%".$palabra."%'
          or registro.referencia_carro like '%".$palabra."%' or registro.valor like '%".$palabra."%'
          or registro.monto_financiar like '%".$palabra."%' or ciudades.ciudad like '%".$palabra."%'
          or ocupaciones.ocupacion like '%".$palabra."%'");
        if ($query->num_rows>0){
          return $query->result_array();
         } else {
          return false;
         }
  }
  

  	

}  	