<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
		

		function __construct(){
			parent::__construct();
			$this->load->model('model_index');
			 require_once(APPPATH.'libraries/excelWriter.php');

		}

		function _logeo_in_admin(){
	      $login_in = $this->session->userdata('login_credi_vehiculos');
	      if ($login_in !=true){
	        redirect('index');
	      }else{
	      	if($this->session->userdata('tipo')==2){
	      	  redirect('admin/registro');	
	      	}
	      }
	    }

	    function _logeo_in_user(){
	      $login_in = $this->session->userdata('login_credi_vehiculos');
	      if ($login_in !=true){
	        redirect('index');
	      }
	      
	    }


	public function index(){
		$this->_logeo_in_admin();
		$user=$this->model_index->user_total();
		$data['user']=$user;
		$this->load->view('admin/user',$data);
	}


	public function registro(){
		$this->_logeo_in_user();
		$registros=$this->model_index->registros();
		$data['registros']=$registros;
		$this->load->view('admin/registros',$data);
	}

	

	public function crear_usuario(){
		$this->_logeo_in_admin();
		$this->load->view('admin/crear_usuario');
	}


	public function insert_user(){
 	    $this->form_validation->set_rules('user','Usuario','required|callback__verificaruser');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm','Password cofrimacion','required');
		$this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('matches','las conraseñas no coinciden');
		$this->form_validation->set_message('_verificaruser','Este usuario ya existe');
		if($this->form_validation->run()==FALSE) {
			$this->crear_usuario();
		}else{  
			$datos=array(
		        'user' => $this->input->post('user'),
		        'password' => md5($this->input->post('password')),
		        'tipo' =>2,

		        );
			 $user=$this->model_index->insert_user($datos);
		     redirect('admin/index');
		}	
		    
 	}


 	public function _verificaruser(){
	    return $this->model_index->verificacion_user($this->input->post('user'));
    }

    public function editar_usuario($id_user){
		$this->_logeo_in_admin();
		$data['user']=$this->model_index->user_id($id_user);
		$this->load->view('admin/editar_usuario',$data);
	}

	public function eliminar_usuario($id_user){
		$this->model_index->eliminar_user($id_user);
        redirect('admin/index');
	}


	public function update_user(){
 	    $this->form_validation->set_rules('id_user','id_user','required');
 	    $this->form_validation->set_rules('user','Usuario','required|callback__verificaruserEditar');
		$this->form_validation->set_rules('password', 'Password', 'matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm','Password cofrimacion');
		$this->form_validation->set_message('required','%s es requerido');
		$this->form_validation->set_message('matches','las conraseñas no coinciden');
		$this->form_validation->set_message('_verificaruserEditar','Este usuario ya existe');
		if($this->form_validation->run()==FALSE) {
			$this->editar_usuario($this->input->post('id_user'));
		}else{  
			$datos=array(
				'id'=>$this->input->post('id_user'),
		        'user' => $this->input->post('user'),
		        'password' => md5($this->input->post('password')),
		        'tipo' =>2,

		        );
			 $user=$this->model_index->update_user($datos);
		     redirect('admin/index');
		}	
		    
 	}

 	public function _verificaruserEditar(){
	    return $this->model_index->verificacion_userEditar($this->input->post('user'),$this->input->post('id_user'));
    }


    public function filtro(){
    	$palabra=$_POST['palabra'];
    	$registros=$this->model_index->registro_like($palabra);
    	$consulta=$this->db->last_query();
        $html='';
    	if($registros){

		   	foreach ($registros as $r) {
		   			$html.='<tr>';
		   				$html.='<td>'.$r['ciudad'].'</td>';
		   				$html.='<td>'.$r['ocupacion'].'</td>';
		   				$html.='<td>'.$r['nombre'].'</td>';
		   				$html.='<td>'.$r['apellido'].'</td>';
		   				$html.='<td>'.$r['correo'].'</td>';
		   				$html.='<td>'.$r['telefono'].'</td>';
		   				$html.='<td>'.$r['celular'].'</td>';
		   				$html.='<td>'.$r['marca_carro'].'</td>';
		   				$html.='<td>'.number_format($r['valor']).'</td>';
		   				$html.='<td>'.number_format($r['monto_financiar']).'</td>';
		   				$html.='<td>'.$r['referencia_carro'].'</td>';
		   			$html.='</tr>';

		   	 } 
		}
        
		$data['html']=$html;
		$data['consulta']=$consulta;
		echo json_encode($data);


    }


   public function excel(){
      $xls = new ExcelWriter();
      $consulta=$_POST['consulta'];
      if($consulta){
      	$query=$this->db->query($consulta);
        if ($query->num_rows>0){
        	$registros = $query->result_array();
	      //$registro=$query->result_array();
	     } else {
	      $registros='';
	     }
      }else{
         $registros=$this->model_index->registros();
      }
      $xls->OpenRow();
      
      $arr = array('Cuidad de residencia','Ocupacion','Nombre','Apellido','Correo Electronico','Telefono de contacto','Celular de contacto','Marca','Valor','Monto a financiar','Referencia'); 
      
     foreach($arr as $cod=>$val)
        $xls->NewCell($val,false,array('align'=>'center','background'=>'666666','color'=>'FFFFFF','bold'=>true,'border'=>'000000'));
      
      $xls->CloseRow();
     if($registros){
          foreach($registros as $row){
                $xls->OpenRow();
                $xls->NewCell($row['ciudad'],false);
   				$xls->NewCell($row['ocupacion'],false);
   				$xls->NewCell($row['nombre'],false);
   				$xls->NewCell($row['apellido'],false);
   				$xls->NewCell($row['correo'],false);
   				$xls->NewCell($row['telefono'],false);
   				$xls->NewCell($row['celular'],false);
   				$xls->NewCell($row['marca_carro'],false);
   				$xls->NewCell(number_format($row['valor']),false);
   				$xls->NewCell(number_format($row['monto_financiar']),false);
   				$xls->NewCell($row['referencia_carro'],false);;
                $xls->CloseRow();
          }
      }   
      $xls->GetXLS(true,'Registros');
  }

	

}	
