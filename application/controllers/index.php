<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
		

		function __construct(){
			parent::__construct();
			$this->load->model('model_index');

		}


	public function index(){
		$marca=$this->input->post('marca');
		$monto=$this->input->post('monto');
		$valor=$this->input->post('valor');
		$referencia=$this->input->post('referencia');


		$ciudades=$this->model_index->ciudades();
		$ocupaciones=$this->model_index->ocupaciones();
		$data['marca']=$marca;
		$data['monto']=$monto;
		$data['valor']=$valor;
		$data['referencia']=$referencia;
		$data['ciudades']=$ciudades;
		$data['ocupaciones']=$ocupaciones;
		$this->load->view('index',$data);
	}
    

    public function registro(){
    	    $this->form_validation->set_rules('ciudad','ciudad','required');
			$this->form_validation->set_rules('nombre','nombre','required');
			$this->form_validation->set_rules('correo','correo','required|valid_email');
			$this->form_validation->set_rules('celular','celular','required');
			$this->form_validation->set_rules('ocupacion','ocupacion','required');
			$this->form_validation->set_rules('apellido','apellido','required');
			$this->form_validation->set_rules('telefono','telefono','required');
			$this->form_validation->set_rules('marca','marca','required');
			$this->form_validation->set_rules('monto','monto','required');
			$this->form_validation->set_rules('valor','valor','required');
			$this->form_validation->set_rules('referencia','referencia','required');
			$this->form_validation->set_rules('auto','auto','required');
			
			
			$this->form_validation->set_message('required','%s es requerido');
			$this->form_validation->set_message('valid_email','Correo no valido');
			if ($this->form_validation->run()==FALSE) {
				$this->index();
			}else{
				$data=array('id_ciudad'=>$this->input->post('ciudad'),
                            'id_ocupacion'=>$this->input->post('ocupacion'),
                            'nombre'=>$this->input->post('nombre'),
                            'apellido'=>$this->input->post('apellido'),
                            'correo'=>$this->input->post('correo'),
                            'telefono'=>$this->input->post('telefono'),
                            'celular'=>$this->input->post('celular'),
                            'marca_carro'=>$this->input->post('marca'),
                            'referencia_carro'=>$this->input->post('referencia'),
                            'valor'=>str_replace(',','',$this->input->post('valor')),
                            'monto_financiar'=>str_replace(',','',$this->input->post('monto')),
                            'autorizacion'=>$this->input->post('auto'),
                            'fecha_registro'=>Date('Y-m-d'),
					        );
				$this->model_index->Insert_registro($data);
				redirect('index/confirmacion');

			}	
    }

    public function confirmacion(){
		$this->load->view('confirmacion');
	}


	public function login($msg=""){
		$data['msg']=$msg;
		$this->load->view('login',$data);
	}

	public function validar_login(){
 	    $this->form_validation->set_rules('user','Usuario','required');
		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_message('required','%s es requerido');
		if($this->form_validation->run()==FALSE) {
			$this->login();
		}else{  
			 $datos=array(
		        'user' => $this->input->post('user'),
		        'password' => md5($this->input->post('password')),

		        );
			 $user=$this->model_index->user($datos);
		     if($user){
		     	$value=array('validacion'=>true);
		     		$session = array(
	                 'login_credi_vehiculos' => TRUE,
	                 'tipo' => $user['0']->tipo,
	                );
	                $this->session->set_userdata($session); 
	                redirect('admin');
		     } else {
		     	redirect('index/login/1');
		     }
		}	
		    
 	}



 	public function login_out(){
	    $this->session->sess_destroy();
	    redirect('index/login');
    }

}
