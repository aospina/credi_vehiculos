<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.css">
	 <meta http-equiv="X-UA-Compatible" content="IE=8"/>

	 <script src="<?php echo base_url() ?>js/jquery_1.9.0.min.js"></script>
	<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
	
   <!--[if lte IE 8]>
    <script src="js/html5shiv.min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
        <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/i8-style.css">
    <![endif]-->
  
	<script>
	$(document).ready(function(){
		$("#form").validate({
  	errorPlacement: function(error, element) {
    	if (element.attr("name") == "auto") {
      			error.insertAfter('.large-11');
    		} else {
      		error.insertAfter(element);
    		}
    	}
  		
		});
		$.validator.messages.required = "Campo Requerido";

	});
		
	</script>
   <!--[if lte IE 8]>
    <script src="js/html5shiv.min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
        <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/ie-style.css">
    <![endif]-->

	<title>Crédito de Vehículo</title>
</head>
<body>
	<div class=" header">
		<div class="row">
			 <div class="large-4 medium-4 small-5 columns"> <img src="<?php echo base_url() ?>img/logo_vehiculo.png" alt=""></div>
			<div class="large-4 medium-4 small-5 columns" ><img src="<?php echo base_url() ?>img/logo_banco.png" alt=""></div>
	  
		</div>
		
	</div>
		
	<div class="row content admin"  style="width : 60%;">
		<?php echo form_open('index/validar_login',array('id'=>'form','name'=>'form')) ?>
		<div class="large-12 medium-12 columns " >
		<h4>Login</h4>
		   <label for="">Usuario:</label>
		   <input type="text" class="required" name="user">
		   <?php echo form_error('user') ?>
		   <label for="">Contraseña:</label>
		   <input type="password" class="required" name="password">
		   <?php echo form_error('password') ?>
		   <?php if($msg==1){ ?>
		                <div class="error">Datos Incorrectos</div>
		      <?php } ?>
		 <div><input type="submit" value="Login" class="button expand"></div>
		 
		<?php echo form_close('') ?>
		</div>	
		
	</div>


	<footer > Todos los derechos reservados © 2015. Crédito de Vehiculo - Banco de Bogotá.</footer>

</body>
</html>