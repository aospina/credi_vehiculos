<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=8"/>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.css">
	 
   <!--[if lte IE 8]>
    <script src="js/html5shiv.min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
        <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/i8-style.css">
    <![endif]-->
	<title>Crédito de Vehículo</title>
</head>
<body>
	<div class=" header">
		<div class="row main">
			 <div class="large-4 medium-4 small-5 columns"> <img src="<?php echo base_url() ?>img/logo_vehiculo.png" alt=""></div>
			<div class="large-4 medium-4 small-5 logo_banco columns" ><img src="<?php echo base_url() ?>img/logo_banco.png" alt=""></div>
	  
		</div>
		
	</div>
	<div class="row no-padding main">
			<img src="<?php echo base_url() ?>img/Banner.jpg" alt="">
	</div>	
	<div class="row content main" >
		<div class="large-8 small-8 center">
			<h1>Gracias por aplicar online.</h1>
			<h2>Entre 1 a 3 días hábiles un asesor se contactará contigo <br>para continuar el proceso.</h2>
		</div>
	</div>


	<footer > Todos los derechos reservados © 2015. Crédito de Vehiculo - Banco de Bogotá.</footer>

</body>
</html>