<!DOCTYPE html>
<html lang="en">
<head>
	 <meta http-equiv="X-UA-Compatible" content="IE=8"/>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.css">
	<script src="<?php echo base_url() ?>js/jquery_1.9.0.min.js"></script>
	<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
	<script src="<?php echo base_url() ?>js/script.js"></script>
	
   <!--[if lte IE 8]>
    <script src="js/html5shiv.min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
        <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/i8-style.css">
    <![endif]-->
  
	<script>
	$(document).ready(function(){

		jQuery.validator.addMethod("letter", function(value, element) {
  			return this.optional(element) || /^[a-z," "]+$/i.test(value);
		}, "Solo Letras"); 

		$("#form").validate({
  			errorPlacement: function(error, element) {
    			
    			if (element.attr("name") == "auto") {
      				error.insertAfter('.large-11');
    			} else {
      				error.insertAfter(element);
    			}
    			
    		},
    		rules: {
   					 nombre: { letter: true },
   					 apellido: { letter:true },

   					 telefono: {minlength: 7 },
   					 celular: {minlength: 10 }

  			},
  			messages: {
  				telefono:{
  					
      				minlength: jQuery.validator.format("Por favor ingrese un número de teléfono válido")
  				},
  				celular:{
  					
      				minlength: jQuery.validator.format("Por favor ingrese un número celular válido")
  				} 

  			}


  		
		});
		$.validator.messages.required = "Campo Requerido";
		$.validator.messages.email = "Por favor escriba un correo electrónico válido";

	});

		

	</script>
	<title>Crédito de Vehículo</title>
</head>
<body>
	<div class=" header">
		<div class="row main">
			 <div class="large-4 medium-4 small-5  columns"> <img src="<?php echo base_url() ?>img/logo_vehiculo.png" alt=""></div>
			<div class="large-4 medium-4  small-5 columns" ><img src="<?php echo base_url() ?>img/logo_banco.png" alt=""></div>
		</div>
		
	</div>
	<div>
	<div class="row main no-padding main ">
			<img src="<?php echo base_url() ?>img/Banner.jpg" alt="">
	</div>
	<div>	
	<?php echo form_open('index/registro',array('id'=>'form','name'=>'form')) ?>
	<div class="row main content main" >
		
		<h5 class="text-center intro" >Completa los siguientes datos para realizar tu aplicación online</h5>
		
		<div class="large-8 medium-8 center">
			<h5>Datos Personales:</h5>
	
			<div class="large-6 medium-6 columns " >
		
				<label for="">Ciudad de Residencia:</label>
				<select name="ciudad" id="" class="required">
				  <option value="">Seleccione una opción</option>
					<?php if($ciudades): ?>
						  <?php foreach ($ciudades as $c) : ?>
						  	<option value="<?php echo $c['id'] ?>"><?php echo $c['ciudad'] ?></option>
						  <?php endforeach ?>
					<?php endif; ?>
					<?php echo form_error('ciudad') ?>
				</select>
				</div>
				<div class="large-6 medium-6 columns" >
					<label for="">Ocupación</label>
					<select name="ocupacion" id="" class="required">
					<option value="">Seleccione una opción</option>
						<?php if($ocupaciones): ?>
							  <?php foreach ($ocupaciones as $o) : ?>
							  	<option value="<?php echo $o['id'] ?>"><?php echo $o['ocupacion'] ?></option>
							  <?php endforeach ?>
						<?php endif; ?>
						<?php echo form_error('ocupacion') ?>
					</select>
				</div>
				<div class="large-6 medium-6 columns" >
					
					<label for="">Nombre:</label>
					<input type="text" class="required"  name="nombre">
					<?php echo form_error('nombre') ?>
				</div>
				<div class="large-6 medium-6 columns" >
					
					<label for="">Apellidos:</label>
					<input type="text" class="required" name="apellido">
					<?php echo form_error('apellido') ?>
				</div>
				<div class="large-6 medium-6 columns" >
					<label for="">Correo electrónico:</label>
					<input type="email" class="required" name="correo">
					<?php echo form_error('correo') ?>
				</div>
				
				<div class="large-6 medium-6 columns" >
					<label for="">Teléfono de Contacto:</label>
					<input type="number" class="required" name="telefono"  requiered>
					<?php echo form_error('telefono') ?>
				</div>
				<div class="large-6 medium-6 columns end" >
					<label for="">Celular de Contacto:</label>
					<input type="number" class="required" name="celular">
					<?php echo form_error('celular') ?>
				</div>
					
			
			</div>
	
	

			<div class="large-8 medium-8 center carro">
				<h5>Datos Del Vehículo:</h5>
				<div class="large-6 medium-6 columns">
		
					<label for="">Marca:</label>
					<?php if($marca){ ?>
					<input type="text" class="required" name="marca" readonly value="<?php echo $marca ?>">
					<?php }else{ ?>
					<input type="text" class="required" name="marca" value="">
					<?php } ?>
					<?php echo form_error('marca') ?>

				</div>
				<div class="large-6 medium-6 columns">
					<label for="">Valor:</label>
					<?php if($valor){ ?>
					<input type="text" class="valor required" name="valor" readonly value="<?php echo number_format($valor) ?>">
					<?php }else{ ?>
					<input type="text" class="valor required" name="valor"   value="">
					<?php } ?>
					<?php echo form_error('valor') ?>
				</div>
				<div class="large-6 medium-6 columns">
					<label for="">Monto a financiar:</label>
					<?php if($monto){ ?>
					<input type="text" class="monto required" name="monto" class="monto" readonly value="<?php echo number_format($monto) ?>">
					<?php }else{ ?>
					<input type="text" class="monto required" name="monto" class="monto" value="">
					<?php } ?>
					<?php echo form_error('monto') ?>
				</div>	
				
				
				<div class="large-6 medium-6 columns">
					<label for="">Referencia:</label>
					<?php if($referencia){ ?>
					<input type="text" class="required" name="referencia" readonly value="<?php echo $referencia ?>">
					<?php }else{ ?>
					<input type="text" class="required" name="referencia" value="">
					<?php } ?>
					<?php echo form_error('referencia') ?>
				</div>
				<div class="large-1 medium-1  no-padding columns"><input type="checkbox" class="required" name="auto">	</div>
				<div class="large-11 medium-11  no-padding columns "><label for="">Autorizo al Banco de Bogotá para contactarme y acepto las condiciones de uso.</label></div>				
				<div class="larege-12 scroll">
					Las presentes condiciones de uso, regulan el servicio de este canal virtual. Para (i) cumplir con normas legales de conocimiento del usuario; (ii) establecer, mantener y profundizar la relación contractual; (iii) actualizar la información; (iv) evaluar el riesgo; (v) profundizar productos y servicios; (vi) determinar el nivel de endeudamiento de manera consolidada; (vii) efectuar labores de mercadeo, investigaciones comerciales o estadísticas; (viii) por razones de seguridad; (ix) prevención de lavado de activos, financiación del terrorismo y cumplimiento de normas legales, y mientras el USUARIO tenga algún producto y/o servicio,responsabilidad directa o indirecta, por el tiempo adicional que exijan normas especiales o por los tiempos de prescripción, el USUARIO expresamente y de manera permanente autoriza al BANCO: a)Para consultar, obtener, recolectar, almacenar, usar, utilizar, intercambiar, conocer, circular o suprimir información financiera, datos personales, comerciales, privados, semiprivados o de cualquier naturaleza del USUARIO que éste suministre o a los que tuviere acceso el BANCO por cualquier medio sin lugar a pagos ni retribuciones. b)Para consultar,obtener, recolectar, almacenar, usar, utilizar, intercambiar, conocer, circular, suprimir y en general enviar y recibir, por cualquier medio la información financiera, dato personal, comercial, privado, semiprivado o de cualquier naturaleza del USUARIO contenido en las bases de datos del BANCO, con su matriz, sus subordinadas, las subordinadas de su matriz (vinculadas) o cualquier sociedad en la que el BANCO tenga o no participación en el capital y viceversa sin lugar a pagos ni retribuciones .c) Para consultar, obtener, recolectar, almacenar, usar, utilizar, intercambiar, conocer, circular, suprimir o divulgar la información financiera, dato personal, comercial, privado o semiprivado, o acerca de operaciones vigentes activas o pasivas o de cualquier naturaleza o las que en el futuro llegue a celebrar el USUARIO con ELBANCO, con otras entidades financieras o comerciales, con cualquier operador o administrador de bancos de datos de información financiera o cualquier otra entidad similar que en un futuro se establezca y que tenga por objeto cualquiera de las anteriores actividades. d)Para consultar, obtener, recolectar, almacenar, analizar, usar, reportar, intercambiar, circular, suprimir o divulgar con carácter permanente a cualquier operador de información, cualquier entidad del sector financiero o real, la matriz, las vinculadas y subordinadas del BANCO la información financiera, dato personal, comercial, privado, semiprivado o de cualquier naturaleza del USUARIO y frente a: (i) información acerca del nacimiento, modificación, celebración y/o extinción de obligaciones directas, contingentes o indirectas del USUARIO; (ii) información acerca del incumplimiento de las obligaciones o de las que cualquiera de estas entidades (entidades del sector financiero o real, la matriz, las vinculadas y subordinadas del BANCO) adquiera a cargo del USUARIO; (iii) cualquier novedad en relación con las obligaciones contraídas por EL USUARIO para con EL BANCO o con cualquiera de sus subordinadas nacionales o extranjeras, entidades del sector financiero o del sector real; o (iv) información referente al endeudamiento, hábitos de pago y comportamiento crediticio con el BANCO y/o terceros con el fin, entre otros de que sea incluido el nombre del USUARIO y su documento de identificación en los registros de deudores morosos o con referencias negativas, su endeudamiento, las operaciones y/o obligaciones vigentes y las que adquiera o las que en el futuro llegare a celebrar cualquiera que sea su naturaleza con EL BANCO o con cualquiera de sus subordinadas, en cualquier operador o administrados de banco de datos de información financiera o cualquier otra entidad similar o que en el futuro se establezca y tenga por objeto la recopilación, procesamiento, consulta y divulgación.La autorización faculta al BANCO no sólo para almacenar, reportar, procesar y divulgar la información a los operadores de información, sino también para que EL BANCO pueda solicitar y consultar información sobre las relaciones comerciales del USUARIO con terceros, con el sector real o financiero, el cumplimiento de sus obligaciones, contratos, hábitos de pago, etc. y para que la información reportada pueda ser actualizada, usada, almacenada y circularizada por el operador de información. e)Para obtener de las fuentes que considere pertinentes información financiera, comercial, personal y/o referencias sobre el manejo de cuentas corrientes, ahorros, depósitos en corporaciones, tarjetas de crédito, comportamiento comercial y demás productos o servicios y, en general, del cumplimiento y manejo de los créditos y obligaciones del USUARIO cualquiera que sea su naturaleza. Las partes convienen que esta autorización comprende la información presente, pasada y futura referente al manejo, estado, cumplimiento de las relaciones, contratos y servicios, hábitos de pago, obligaciones y las deudas vigentes, vencidas sin cancelar, procesos, o a la utilización indebida de los servicios financieros del USUARIO Todo lo anterior mientras estén vigentes y adicionalmente por el término máximo de permanencia de los datos en las Centrales de Riesgo, de acuerdo con los pronunciamientos de la Corte Constitucional o de la Ley, contados desde cuando extinga la obligación o relación, este último plazo para los efectos previstos en los artículos 1527 y SS del C.C. y 882 del C. de CO.f)Para que en caso de que quede algún saldo insoluto de alguna obligación o contingencia por cualquier concepto, de cualquiera naturaleza y/o servicio éste se lleve a una cuenta por cobrar a cargo del USUARIO y dicha obligación sea reportada a cualquier operador de información, así como su incumplimiento, tiempo de mora, etc. g)Para enviar mensajes que contengan información comercial, de mercadeo, personal, institucional, de productos o servicios o de cualquier otra índole que el BANCO considere al teléfono móvil y/o celular, correo electrónico, correo físico o por cualquier otro medio. h)Para que si suministro datos sensibles el BANCO con carácter permanente pueda recolectar, almacenar, usar, circular, suprimir o intercambiar dichos datos sin lugar a pagos ni retribuciones. Se consideran como datos sensibles además de las consagradas en la Ley las fotos, grabaciones y/o videograbaciones que el USUARIO realice con ocasión de cualquier operación, gestión y/o visita, las cuales autoriza realizar y además para que puedan ser utilizados como medio de prueba. i)Para la recolección, uso, almacenamiento, circulación, transferencia, intercambio o supresión de los datos personales, comerciales, privados, semiprivados o sensibles del USUARIO con terceros países o entidades de naturaleza pública o privada internacionales y/o extranjeras. j)Si aplica, para que recolecte y entregue la información financiera, demográfica, datos personales, comerciales, privados, fiscales, semiprivados o de cualquier naturaleza del USUARIO en cumplimiento de regulación de autoridad extranjera, lo mismo que para efectuar las retenciones que igualmente ordenen como consecuencia de los requerimientos u órdenes de tales autoridades, todo lo anterior siempre y cuando le sean aplicables las disposiciones FATCA (ForeignAccountTax ComplianceAct) u otras órdenes similares emitidas por otros Estados. k)Para que la información financiera, datos personales, comerciales, privados, semiprivados o sensibles recolectados o suministrados por el USUARIO o por terceros por cualquier medio, pueda ser utilizada como medio de prueba. Para el ejercicio de los derechos que le confiere la Ley (Ley 1581 de 2012 y demás normas concordantes) y si fuera aplicable, el USUARIO manifiesta que ha sido informado y que conoce que podrá actuar personalmente, por escrito y/o por cualquier otro medio técnico idóneo que resulte aceptable y que el banco le informe o ponga a sus disposiciones en la página web www.bancodebogota.com l) El(los) USUARIO(S) se obliga a mantener vacunados los equipos con antivirus de reconocido valor técnico y debidamente actualizados. m) El(los) USUARIO(S) es responsable de los perjuicios que por su culpa se causen al BANCO o a terceros por el uso indebido del CANAL VIRTUAL, así como por la falta de veracidad, inexactitud, omisión y/o error de la información suministrada por el(los) USUARIO(S). n) El(los) USUARIO(S) acepta(n) que se registre la dirección IP desde la que se accede al CANAL VIRTUAL. El banco queda facultado para terminar, suspender, bloquear estos servicios por razones objetivas. o) El USUARIO asume los perjuicios que se causen como consecuencia directa e indirecta, del mal uso o interpretación de la información suministrada por el(los) USUARIO(S) a través del CANAL VIRTUAL.
				</div>
				<input type="submit" value="Aplicar Online" class="button expand">
			</div>
		</div>

	<?php echo form_close('') ?>
	</div>
	<footer > Todos los derechos reservados © 2015. Crédito de Vehiculo - Banco de Bogotá.</footer>

</body>
</html>