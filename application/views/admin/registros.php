<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.css">
	<script src="<?php echo base_url() ?>js/jquery_1.9.0.min.js"></script>
	<script type="text/javascript"> var site_url="<?php echo base_url(); ?>"</script>
	<script src="<?php echo base_url() ?>js/script.js"></script>
	<script src="<?php echo base_url() ?>js/jquery_1.9.0.min.js"></script>
	 <meta http-equiv="X-UA-Compatible" content="IE=8"/>


	<title>Crédito de Vehículo</title>
</head>
<body>
	<div class=" header">
		<div class="row">
			 <div class="large-4 medium-4 small-5 columns"> <img src="<?php echo base_url() ?>img/logo_vehiculo.png" alt=""></div>
			<div class="large-4 medium-4 small-5 columns" ><img src="<?php echo base_url() ?>img/logo_banco.png" alt=""></div>
	  
		</div>
		
	</div>
		
	<div class="row content admin" >
		<div class="large-12 medium-12 columns " >
		  <h4>Registros</h4>
		  <ul class="buscar">
		  	<li><a href="#" class="imprimir">Descargar</a></li>
		  	<?php if($this->session->userdata('tipo')==1){ ?>
		 	<li><a href="<?php echo base_url() ?>admin/index">Usuarios</a></li>
		  	<?php } ?>
		  	<li><a href="<?php echo base_url() ?>index/login_out">Cerrar sesión</a></li>
		  	<li style="text-align:right"><span class="search"></span><input value="" name="buscar" class="buscar" hidden="true"></li>
		  </ul>
		  <div class="tabla">
            <table>
	            	<thead>
	            		<tr>
	            			<td>Cuidad</td>
	            			<td>Ocupación</td>
	            			<td>Nombre</td>
                            <td>Apellido</td>
	            			<td>Correo Electrónico</td>
	            			<td>Teléfono</td>
	            			<td>Celular</td>
	            			<td>Marca </td>
	            			<td>Valor Vehículo</td>
	            			<td>Monto a financiar</td>
	            			<td>Referencia</td>
	            		</tr>
	            	</thead>
           
				   <?php if($registros): ?>
				   	<tbody class="resultado">
				   		<?php foreach ($registros as $r) { ?>
				   			<tr>
				   				<td><?php echo $r['ciudad'] ?></td>
				   				<td><?php echo $r['ocupacion'] ?></td>
				   				<td><?php echo $r['nombre'] ?></td>
				   				<td><?php echo $r['apellido'] ?></td>
				   				<td><?php echo $r['correo'] ?></td>
				   				<td><?php echo $r['telefono'] ?></td>
				   				<td><?php echo $r['celular'] ?></td>
				   				<td><?php echo $r['marca_carro'] ?></td>
				   				<td><?php echo number_format($r['valor']) ?></td>
				   				<td><?php echo number_format($r['monto_financiar']) ?></td>
				   				<td><?php echo $r['referencia_carro'] ?></td>
				   			</tr>

				   		<?php } ?>
				   	</tbody>
				   <?php endif; ?>
		    	</table>
				</div>
		    	<?php echo form_open('admin/excel','id=form_excel') ?>
		    	   <input  type="hidden" value="" class="consulta" name="consulta">
		    	<?php echo form_close(); ?>

		 </div>
	</div>


	<footer > Todos los derechos reservados © 2015. Crédito de Vehiculo - Banco de Bogotá.</footer>
	<script>
		$(".search").click(function(){
			$(this).next().animate({width: 'toggle'});
		})
	</script>
</body>
</html>