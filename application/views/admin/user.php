<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.css">
	
	 <script src="<?php echo base_url() ?>js/jquery_1.9.0.min.js"></script>
	<script type="text/javascript"> var site_url="<?php echo base_url(); ?>"</script>
	<script src="<?php echo base_url() ?>js/script.js"></script>
	 <meta http-equiv="X-UA-Compatible" content="IE=8"/>
    <script type="text/javascript">var base_url='<?php echo base_url() ?>';</script>

	<title>Crédito de Vehículo</title>
</head>
<body>
	<div class=" header">
		<div class="row">
			 <div class="large-4 medium-4 small-5 columns"> <img src="<?php echo base_url() ?>img/logo_vehiculo.png" alt=""></div>
			<div class="large-4 medium-4 small-5 columns" ><img src="<?php echo base_url() ?>img/logo_banco.png" alt=""></div>
	  
		</div>
		
	</div>
		
	<div class="row content admin"  >
		<div class="large-12 medium-12 columns " >
		  <h4>User</h4>
		  <ul>
		  	<li><a href="<?php echo base_url() ?>admin/crear_usuario">Crear Usuario</a></li>
		  	<li><a href="<?php echo base_url() ?>admin/registro">Ver Registros</a></li>
		  	<li><a href="<?php echo base_url() ?>index/login_out">Cerrar Sesión</a></li>
		  </ul>
		  
            <table>
	            	<thead>
	            		<tr>
	            			<td>Usuario</td>
	            			<td>Accion</td>
	            		</tr>
	            	</thead>
           
				   <?php if($user): ?>
				   	<tbody>
				   		<?php foreach ($user as $u) { ?>
				   			<tr>
				   				<td><?php echo $u['user'] ?></td>
				   				<td><a href="<?php echo base_url('admin/editar_usuario/'.$u['id']) ?>" data-id="<?php echo $u['id'] ?>" class="">Editar</a>                /
				   				<a href="#" data-iduser="<?php echo $u['id'] ?>" class="eliminar">Eliminar</a></td>
				   			</tr>

				   		<?php } ?>
				   	</tbody>
				   <?php else: ?>
				   	<tr>
				   		<td><h5>No Hay Usuarios</h5></td>
				   	</tr>
				   <?php endif; ?>
		    	</table>

		 </div>
	</div>


	<footer > Todos los derechos reservados © 2015. Crédito de Vehiculo - Banco de Bogotá.</footer>

</body>
</html>