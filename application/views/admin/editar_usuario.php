<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/foundation.css">
	 <meta http-equiv="X-UA-Compatible" content="IE=8"/>

	 <script src="<?php echo base_url() ?>js/jquery_1.9.0.min.js"></script>
	<script src="<?php echo base_url() ?>js/jquery.validate.js"></script>
	
   <!--[if lte IE 8]>
    <script src="js/html5shiv.min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
        <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/i8-style.css">
    <![endif]-->
  
	<script>
	$(document).ready(function(){
		$("#form").validate({
  	errorPlacement: function(error, element) {
    	if (element.attr("name") == "auto") {
      			error.insertAfter('.large-11');
    		} else {
      		error.insertAfter(element);
    		}
    	}
  		
		});
		$.validator.messages.required = "Campo Requerido";

	});
		
	</script>
   <!--[if lte IE 8]>
    <script src="js/html5shiv.min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
        <!--[if lt IE 9]>
    <link rel="stylesheet" href="css/ie-style.css">
    <![endif]-->

	<title>Crédito de Vehículo</title>
</head>
<body>
	<div class=" header">
		<div class="row">
			 <div class="large-4 medium-4 small-5 columns"> <img src="<?php echo base_url() ?>img/logo_vehiculo.png" alt=""></div>
			<div class="large-4 medium-4 small-5 columns" ><img src="<?php echo base_url() ?>img/logo_banco.png" alt=""></div>
	  
		</div>
		
	</div>
		
	<div class="row content admin" style="width : 40%;" >
		<?php echo form_open('admin/update_user',array('id'=>'form','name'=>'form')) ?>
		<div class="large-12 medium-12 columns " >
		<h5>Editar Usuario</h5>
		   <label for="">Usuario:</label>
		   <input type="text" class="required" name="user" value="<?php echo $user['0']->user ?>">
		   <input type="hidden" class="required" name="id_user" value="<?php echo $user['0']->id ?>">
		   <?php echo form_error('user') ?>
		   <label for="">Contraseña:</label>
		   <input type="password" class="" name="password">
		   <?php echo form_error('password') ?>
		   <label for="">Confirme Contraseña:</label>
		   <input type="password" class="" name="password_confirm">
		   <?php echo form_error('password_confirm') ?>
		   <input type="submit" value="Editar Usuario" class="button expand">
		
		</div>	

		 <?php echo form_close('') ?>
	</div>


	<footer > Todos los derechos reservados © 2015. Crédito de Vehiculo - Banco de Bogotá.</footer>

</body>
</html>