$(document).ready(function() {
     
     $('.buscar').keyup(function(e){
	      if (e.keyCode == 13){
	      	   datos= {palabra: $(this).val()}
				$.ajax({
					type: "POST",
					url: site_url+"/admin/filtro",
					data: datos,
					dataType: "json",
					success:function(data){
		                   if(data.html!=''){
		                   	 $('.resultado').html(data.html);
		                   	 $('.consulta').val(data.consulta);
		                   }else{
		                   	 var html='<tr><td colspan=11>No hay resultados</td></tr>';
		                   	 $('.resultado').html(html);
		                   }
					}
				}); 
	      }
      });

     $('.eliminar').click(function(e){
     	var id_user=$(this).attr('data-iduser');
     	if(id_user){
     	   if(confirm('Desea eliminar este usuario')){
             window.location.href=site_url+'admin/eliminar_usuario/'+id_user;
     		}	
     	}
     	
	      	   
      });

      $('.imprimir').click(function(){
      	  $('#form_excel').submit();
      })	

      function format(input){
			var num = input.toString().replace(/\./g,'');
			num  = num.replace('$ ', '');
			if(!isNaN(num)){
				num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
				num = num.split('').reverse().join('').replace(/^[\.]/,'');
				return '$ ' + num;
			}else{
				//input.value = '';
				return '';
			}
		}



	 	 	$('.monto').on('keyup', function(){
			$('.monto').val(format(this.value));
		});
	 	 	$('.valor').on('keyup', function(){
			$('.valor').val(format(this.value));
		});


});
