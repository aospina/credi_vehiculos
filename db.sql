CREATE DATABASE  IF NOT EXISTS `credi_vehiculo` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `credi_vehiculo`;
-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: credi_vehiculo
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ciudad` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudades`
--

LOCK TABLES `ciudades` WRITE;
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` VALUES (1,'Aguachica'),(2,'Andes'),(3,'Apartado'),(4,'Arauca'),(5,'Armenia'),(6,'Barrancabermeja'),(7,'Barranquilla'),(8,'Bello'),(9,'Bogotá'),(10,'Bucaramanga'),(11,'Caldas'),(12,'Cali'),(13,'Carmen de Viboral'),(14,'Cartagena'),(15,'Caucasia'),(16,'Chía'),(17,'Chiquinquira'),(18,'Cucuta'),(19,'Duitama'),(20,'Envigado'),(21,'Espinal'),(22,'Facatativa'),(23,'Florencia'),(24,'Fusagasuga'),(25,'Girardot'),(26,'Guadalajara de Buga'),(27,'Ibague'),(28,'Itagui'),(29,'Leticia'),(30,'Manizales'),(31,'Medellín'),(32,'Monteria'),(33,'Neiva'),(34,'Ocaña'),(35,'Palmira'),(36,'Pamplona'),(37,'Pasto'),(38,'Pereira'),(39,'Popayan'),(40,'Puerto Berrio'),(41,'Puerto Tejada'),(42,'Quibdo'),(43,'Riohacha'),(44,'Rionegro'),(45,'Sabaneta'),(46,'San Andres'),(47,'San Gil'),(48,'Santa Fe de Antioquia'),(49,'Santa Marta'),(50,'Santa Rosa de Cabal'),(51,'Santa Rosa de Osos'),(52,'Sincelejo'),(53,'Socorro'),(54,'Sogamoso'),(55,'Tulua'),(56,'Tunja'),(57,'Turbo'),(58,'Ubate'),(59,'Valledupar'),(60,'Villavicencio'),(61,'Yopal'),(62,'Zarzal'),(63,'Buenaventura'),(112,'Otro'),(113,'Cartago'),(114,'Cerete'),(115,'Cienaga'),(116,'Copacabana'),(117,'Cota'),(118,'Coveas (Sucre)'),(119,'Honda'),(120,'Madrid'),(121,'Marinilla'),(122,'Mocoa'),(123,'Mosquera'),(124,'Nilo'),(125,'Pensilvania'),(126,'Puerto asis'),(127,'Roldanillo'),(128,'San Juan del Cesar'),(129,'Sevilla'),(130,'Sibundoy'),(131,'Soledad');
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocupaciones`
--

DROP TABLE IF EXISTS `ocupaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocupaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ocupacion` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocupaciones`
--

LOCK TABLES `ocupaciones` WRITE;
/*!40000 ALTER TABLE `ocupaciones` DISABLE KEYS */;
INSERT INTO `ocupaciones` VALUES (1,'Empleado'),(2,'Independiente'),(3,'Prestación de servicios'),(4,'Pensionado');
/*!40000 ALTER TABLE `ocupaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registro`
--

DROP TABLE IF EXISTS `registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ciudad` int(11) DEFAULT NULL,
  `id_ocupacion` int(11) DEFAULT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `correo` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `celular` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `marca_carro` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `referencia_carro` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  `monto_financiar` int(11) DEFAULT NULL,
  `autorizacion` int(11) DEFAULT NULL,
  `fecha_registro` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_registro_1_idx` (`id_ciudad`),
  KEY `fk_registro_2_idx` (`id_ocupacion`),
  CONSTRAINT `fk_registro_1` FOREIGN KEY (`id_ciudad`) REFERENCES `ciudades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registro_2` FOREIGN KEY (`id_ocupacion`) REFERENCES `ocupaciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registro`
--

LOCK TABLES `registro` WRITE;
/*!40000 ALTER TABLE `registro` DISABLE KEYS */;
INSERT INTO `registro` VALUES (1,1,1,'alex','Ospina','alexander.ospina39@gmail.com','2072780','98989989','renault','sandero',29400000,29400000,1,NULL),(2,3,2,'assddas','gmail','alexander.ospina39@gmail.com','2072780','2222222','renault','sandero',29400000,29400000,1,NULL),(3,9,2,'Pablo ','aguero','PABLO@CINCOVEINTICINCO.COM','6231021','3212087257','Renault','Sandero',29000000,29000000,0,'2015-05-04'),(4,1,1,'12344','123','p@p.co','1234','123','Renault','Sandero',29000000,29000000,0,'2015-05-04'),(5,1,1,'12','12','p@pas.com','12313','1231','Renault','Sandero',29000000,29000000,0,'2015-05-04');
/*!40000 ALTER TABLE `registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','202cb962ac59075b964b07152d234b70',1),(2,'alex','202cb962ac59075b964b07152d234b70',2),(3,'rrrrr','202cb962ac59075b964b07152d234b70',2),(4,'carlos222','25f9e794323b453885f5181f1b624d0b',2),(5,'b','c4ca4238a0b923820dcc509a6f75849b',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-04  9:35:51
